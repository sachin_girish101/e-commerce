import React from 'react'

const Navbar = () => {
  return (
    <div className='h-14 w-full bg-black flex justify-start items-center px-4 fixed top-0 z-50'>
      <div className='text-white font-bold text-xl'>My Shop</div>
    </div>
  )
}

export default Navbar