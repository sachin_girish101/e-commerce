import Image from 'next/image';
import Link from 'next/link';
import React from 'react';

const SingleProduct = ({ data }) => {
  return (
    <div className='relative flex h-[35rem] w-[30rem] flex-col bg-gray-600'>
      <div className='absolute top-5 right-10 z-10 flex h-10 w-28 items-center justify-center rounded-full bg-purple-800 text-white'>
        {data.collection}
      </div>
      <Link href={`/products/${data.handle}`}>
      <div className='relative h-[30rem] w-full bg-gray-400 cursor-pointer'>
        <Image
          src={data.images[0].src}
          layout='fill'
          alt='product'
          objectFit='cover'
        />
      </div>
      </Link>

      <div className='flex h-[5rem] w-full items-center justify-between px-4 text-white'>
        <div>{data.title}</div>
        <div>{`₹${data.variants[0].price.amount}`}</div>
      </div>
    </div>
  );
};

export default SingleProduct;
