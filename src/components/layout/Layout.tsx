import * as React from 'react';

import Navbar from '@/components/navbar/Navbar';

export default function Layout({ children }: { children: React.ReactNode }) {
  // Put Header or Footer Here
  return (
    <>
      <Navbar />
      {children}
    </>
  );
}
