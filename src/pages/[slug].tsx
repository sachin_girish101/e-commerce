import React from 'react';

import langs from '../../languages.json';

export async function getStaticPaths() {
  const comb = [];
  for (let i = 0; i < langs.length; i++) {
    for (let j = 0; j < langs.length; j++) {
      if (i !== j) {
        comb.push(langs[i] + '-to-' + langs[j]);
      }
    }
  }
  const paths = comb.map((lang) => {
    return { params: { slug: lang } };
  });
  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  console.log(params);
  return {
    props: { lang: params.slug },
  };
}

const languages = ({ lang }) => {
  return <div>{lang}</div>;
};

export default languages;
