import Image from 'next/image';
import React from 'react';

import { parseShopifyResponse, shopifyClient } from '@/lib/shopify';

import Layout from '@/components/layout/Layout';

export const getServerSideProps = async ({ params }) => {
  const { slug } = params;
  // Fetch one product
  const product = await shopifyClient.product.fetchByHandle(slug);

  return {
    props: {
      product: parseShopifyResponse(product),
    },
  };
};

const ProductPage = ({ product }) => {
  console.log(product);
  return (
    <Layout>
      <div className='mx-auto my-20 max-w-screen-lg flex-row gap-6 px-10 md:flex'>
        <div className='relative h-[30rem] w-[20rem] '>
          <Image
            alt='product'
            src={product.images[0].src}
            layout='fill'
            objectFit='cover'
          />
        </div>
        <div>
          <div className='my-4 text-2xl font-semibold capitalize'>
            {product.title}
          </div>
          <div>{product.description}</div>
          <div className='my-6 text-3xl font-bold'>{`₹${product.variants[0].price.amount}`}</div>
          <button
            className='bg-blue-800 text-white px-4 py-2 rounded-full'
          >
            add to cart
          </button>
        </div>
      </div>
    </Layout>
  );
};

export default ProductPage;
