import * as React from 'react';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import SingleProduct from '@/components/single-product/SingleProduct';

import { parseShopifyResponse, shopifyClient } from '../lib/shopify';

/**
 * SVGR Support
 * Caveat: No React Props Type.
 *
 * You can override the next-env if the type is important to you
 * @see https://stackoverflow.com/questions/68103844/how-to-override-next-js-svg-module-declaration
 */

// !STARTERCONF -> Select !STARTERCONF and CMD + SHIFT + F
// Before you begin editing, follow all comments with `STARTERCONF`,
// to customize the default configuration.

export default function HomePage({ products }) {
  console.log(products);
  return (
    <Layout>
      {/* <Seo templateTitle='Home' /> */}
      <Seo />

      <div className='mt-32 flex flex-wrap justify-around gap-10 px-10'>
        {products.map((el, index) => {
          return <SingleProduct data={el} key={index} />;
        })}
      </div>
    </Layout>
  );
}

export const getServerSideProps = async () => {
  // Fetch all the products
  const products = await shopifyClient.product.fetchAll();
  return {
    props: {
      products: parseShopifyResponse(products),
    },
  };
};
