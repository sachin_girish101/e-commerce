import React, { useState } from 'react';

const Test = () => {
  const [modal, setModal] = useState(false);
  return (
    <div className='relative h-[2000px] transition-all duration-500'>
      <button
        type='button'
        className='absolute z-10 '
        onClick={() => setModal(!modal)}
      >
        click
      </button>
      <div
        className={`bg-black transition-colors ${
          modal ? 'bg-opacity-50' : 'bg-opacity-0 '
        } absolute top-0 -z-0 h-screen w-full duration-500`}
      >
        {modal && (
          <div className='animate-kickout absolute left-1/2 top-1/2 h-[300px] w-[75%] -translate-x-1/2 -translate-y-1/2 bg-white'>
            <button
              className='absolute top-10 right-10'
              onClick={() => setModal(false)}
            >
              x
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default Test;
